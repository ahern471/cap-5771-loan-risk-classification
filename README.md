# CAP-5771-loan-risk-classification

Analyzing P2P loan application data and aim to develop classification algorithms that can predict whether a borrower will be able to repay a loan back based on a low risk vs high risk assessment.