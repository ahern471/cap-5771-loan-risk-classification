I took the Lending-accepted dataset and loaded it entirely into a PostgreSQL table. 
Keep in mind I have the dataset that contains the lending data from 2007 - 2018 so it's bigger than the one you have here.

Then I exported the result of the query below to the lending-accpeted-transformed.csv file and used it as input to the classifier.

The reason why I only selected these features is because since what we are doing is predicting if a borrower could default
on their loan, I'm only selecting what I considered appropriate for this. 

--transforming it to 1s and 0s so it can run in the desicion tree classifier

select loan_amnt, int_rate, installment, annual_inc, dti, fico_range_high, inq_last_6mths,
case when term = ' 36 months' then 36 else 60 end as "term",
case when verification_status = 'Not Verified' then 0 else 1 end as "verification_status",
case when loan_status in ('Fully Paid', 'Current') then 0 else 1 end as "risk",
case when home_ownership = 'MORTGAGE' then 1 else 0 end as "current_mortgage",
case when home_ownership = 'OWN' then 1 else 0 end as "paid_home",
case when home_ownership = 'RENT' then 1 else 0 end as "rents"
from lending_data 
where application_type = 'Individual' --excluding joint (married) applications
and "index" not in (1014615, 1099335) --excluded some bad data here
and loan_status not in ('Does not meet the credit policy. Status:Charged Off', 'Does not meet the credit policy. Status:Fully Paid') --excluding bad data
and home_ownership in ('MORTGAGE', 'OWN', 'RENT') --limiting to these only